<?php

declare(strict_types=1);

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

final class UserInput
{
    #[Assert\NotBlank(groups: ['postUserValidation', 'putUserValidation'])]
    #[Assert\Email(groups: ['postUserValidation', 'putUserValidation'])]
    private ?string $email = null;

    private ?array $roles = null;

    #[Assert\NotBlank(groups: ['postUserValidation', 'putUserValidation'])]
    #[Assert\Length(min: 8, groups: ['postUserValidation', 'putUserValidation'])]
    private ?string $plainPassword = null;

    #[Assert\NotBlank(groups: ['postUserValidation', 'putUserValidation'])]
    #[Assert\Length(min: 3, groups: ['postUserValidation', 'putUserValidation'])]
    private ?string $firstName = null;

    #[Assert\NotBlank(groups: ['postUserValidation', 'putUserValidation'])]
    #[Assert\Length(min: 3, groups: ['postUserValidation', 'putUserValidation'])]
    private ?string $lastName = null;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
