<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

final class UserInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private PasswordHasherFactoryInterface $passwordHasherFactory,
        private ValidatorInterface $validator
    )
    {
    }

    public function transform($data, string $to, array $context = []): User
    {
        $this->validator->validate($data);

        $user = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE] ?? new User();
        $userPasswordHasher = $this->passwordHasherFactory->getPasswordHasher($user);
        $user
            ->setEmail($data->getEmail() ?? $user->getEmail())
            ->setRoles($data->getRoles() ?? $user->getRoles())
            ->setPassword($data->getPlainPassword() ? $userPasswordHasher->hash($data->getPlainPassword()) : $user->getPassword())
            ->setFirstName($data->getFirstName() ?? $user->getFirstName())
            ->setLastName($data->getLastName() ?? $user->getLastName());

        return $user;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof User) {
            return false;
        }

        return User::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
