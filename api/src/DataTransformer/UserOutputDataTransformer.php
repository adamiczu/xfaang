<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\UserOutput;
use App\Entity\User;

final class UserOutputDataTransformer implements DataTransformerInterface
{
    public function transform($data, string $to, array $context = []): UserOutput
    {
        $output = new UserOutput();
        $output
            ->setId($data->getid())
            ->setEmail($data->getEmail())
            ->setRoles($data->getRoles())
            ->setUsername($data->getUsername())
            ->setFirstName($data->getFirstName())
            ->setLastName($data->getLastName());

        return $output;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return UserOutput::class === $to && $data instanceof User;
    }
}
