<?php

declare(strict_types=1);

namespace App\DataFixtures\Faker\Provider;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class HashPasswordProvider
{
    public function __construct(
        private PasswordHasherFactoryInterface $passwordHasherFactory,
    )
    {
    }

    public function hashPassword(string $plainPassword): string
    {
        return $this->passwordHasherFactory->getPasswordHasher(new User())->hash($plainPassword);
    }
}
