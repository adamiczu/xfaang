<?php

namespace App\Tests\Api;

use App\Entity\User;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class AuthenticationTest extends AbstractTest
{
    public function testLogin(): void
    {
        self::createClient()->request('GET', '/users');
        $this->assertResponseStatusCodeSame(401);

        $this->createClientWithCredentials()->request('GET', '/users');
        $this->assertResponseIsSuccessful();
    }
}
