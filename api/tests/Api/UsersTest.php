<?php

namespace App\Tests\Api;

use App\Entity\User;

class UsersTest extends AbstractTest
{
    public function testGetCollection()
    {
        $this->createClientWithCredentials()->request('POST', '/users', ['json' => [
            'email' => 'krzysztof.kowalski@gmail.com',
            'plainPassword' => 'hasloKowalskiego25^',
            'firstName' => 'Krzysztof',
            'lastName' => 'Kowalski',
        ]]);
        $this->assertResponseIsSuccessful();

        $this->createClientWithCredentials($this->getToken([
            'email' => 'krzysztof.kowalski@gmail.com',
            'password' => 'hasloKowalskiego25^',
        ]))->request('GET', '/users');

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(User::class);
    }

    public function testCreateUser()
    {
        $this->createClientWithCredentials()->request('POST', '/users', ['json' => [
            'email' => 'jan.nowak@gmail.com',
            'plainPassword' => 'hasloNowaka25^',
            'firstName' => 'Jan',
            'lastName' => 'Nowak',
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains([
            '@context' => [
                '@vocab' => 'http://example.com/docs.jsonld#',
                'hydra' => 'http://www.w3.org/ns/hydra/core#',
                'id' => 'UserOutput/id',
                'email' => 'UserOutput/email',
                'roles' => 'UserOutput/roles',
                'username' => 'UserOutput/username',
                'firstName' => 'UserOutput/firstName',
                'lastName' => 'UserOutput/lastName',
            ],
            '@type' => 'User',
            'email' => 'jan.nowak@gmail.com',
            'firstName' => 'Jan',
            'lastName' => 'Nowak',
        ]);
    }
}
