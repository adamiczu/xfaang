## Info

Do wykonania zadania użyłem projektu API Platform. Sprawdzone rozwiązanie przez wielu użytkowników Symfony. Wiele standardów, dobre wsparcie społeczności, szybkość w tworzeniu aplikacji i wiele więcej :)

Wszystkie endpointy powiązane z userem wymagają uwierzytelnienia tokenem JWT. 

1. Dane uwierzytelniające można podjerzeć w api/fixtures/users.yaml
2. Adres do testowania API: https://localhost/docs

## Instalacja

1. git clone git@gitlab.com:adamiczu/xfaang.git
2. cd xfaang
3. docker-compose build --pull --no-cache

## Uruchomienie

1. docker-compose up -d

## Konfiguracja

1. docker-compose exec php sh -c '
    set -e
    apk add openssl
    mkdir -p config/jwt
    jwt_passphrase=${JWT_PASSPHRASE:-$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')}
    echo "$jwt_passphrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    echo "$jwt_passphrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
    setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
    setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
'
2. docker-compose exec php bin/console doctrine:migrations:migrate
3. docker-compose exec php bin/console hautelook:fixtures:load

## Testy

1. docker-compose exec php bin/phpunit
